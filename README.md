# Introdução a Engenharia de Computação

Este livro está sendo escrito pelos alunos do Curso de Engenharia da Computação da Universidade Tecnológica do Paraná. O livro é faz parte da disciplina de Introdução a Engenharia e os alunos que participaram da edição estão [listados abaixo](#Autores).

## Índice 

1. [Introdução](capitulos/Introducao.md)
1. [Desenvolvimento](capitulos/Desenvolvimento.md)
1. [Uso no dia a dia](capitulos/Uso_dia_dia.md)
1. [Mercado de trabalho](capitulos/Mercado_trabalho.md)
1. [Referências](capitulos/referencias.md)




## Autores
Esse livro foi escrito por:

| Avatar | Nome | Nickname | Email |
| ------ | ---- | -------- | ----- |
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168526/avatar.png?width=400)  | Jamerson Gabriel Renosto | jamelao.gabriel04 | [jamelao.gabriel04@gmail.com](mailto:jamelao.gabriel04@gmail.com)
| ![]()  | CLEVER MEIRELES LOPES JUNIOR | Clever5 | [clever@alunos.utfpr.edu.br](mailto:clever@alunos.utfpr.edu.br)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9235992/avatar.png?width=400)  | Rafael Hein | RbHein | [hein.rafael@hotmail.com](mailto:hein.rafael@hotmail.com)
| ![](https://gitlab.com/uploads/-/system/user/avatar/9168600/avatar.png?width=400)  | Pedro Lazarete Stranieri | PedroLazarete | [pedrolaza13@gmail.com](mailto:pedrolaza13@gmail.com)
